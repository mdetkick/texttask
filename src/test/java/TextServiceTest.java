import com.student.text.entity.Text;
import com.student.text.entity.TextComponent;
import com.student.text.entity.sentence.Sentence;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Test
public class TextServiceTest extends Assert {

    private TextComponent text;
    private TextComponent text1;
    private List<TextComponent> textComponents;
    private List<Sentence> sentences;

    @BeforeTest
    private void setUp(){
        text = new Text();
        text1 = new Text();
        text.setValue(" asd asd asd ads. asd, asd, asd! asd: asd? aasd.");
        text1.setValue(" aasd. ads: ads? asd, asd, ads! asd asd asd ads.");
        textComponents = text.divide();
        sentences = getSortedSentences();
    }

    @AfterTest
    private void clear(){
        textComponents = null;
        sentences = null;
    }


    private List<Sentence> getSortedSentences(){
        List<Sentence> sentencesConverted = new ArrayList<>();
        for(TextComponent tc : text1.divide()){
            Sentence sentence = (Sentence) tc;
            sentencesConverted.add(sentence);
        }
        return sentencesConverted;
    }

    @Test
    public void shouldSortSentencesByWordsCount(){
        List<Sentence> textComponentsWithCountWords = new ArrayList<>();
        for(TextComponent tc : textComponents){
            Sentence sentence = (Sentence) tc;
            textComponentsWithCountWords.add(sentence);
        }
        textComponentsWithCountWords.sort(Comparator.comparingInt(Sentence::countWords));
        int a1 = sentences.get(0).countWords();
        int a2 = textComponentsWithCountWords.get(0).countWords();
        assertEquals(a1,a2);
    }
}
