package com.student.text.logic;

import com.student.text.entity.Text;
import com.student.text.entity.TextComponent;
import com.student.text.entity.sentence.Sentence;

import java.util.*;

public class TextService {
    private TextComponent text;
    private List<TextComponent> textComponents;

    public TextService(TextComponent text) {
        this.text = text;
        textComponents = text.divide();
    }

    public Text compositeByWordsInSentence(){
        Text text = new Text();
        StringBuilder sentencesBuilder = new StringBuilder();
        for(Sentence sentence : sortSentencesByCountWords()){
            sentencesBuilder.append(sentence.getValue());
        }
        text.setValue(sentencesBuilder.toString());
        return text;
    }

    private List<Sentence> sortSentencesByCountWords(){
        List<Sentence> textComponentsWithCountWords = new ArrayList<>();
        for(TextComponent tc : textComponents){
            Sentence sentence = (Sentence) tc;
            textComponentsWithCountWords.add(sentence);
        }
        textComponentsWithCountWords.sort(Comparator.comparingInt(Sentence::countWords));
        return textComponentsWithCountWords;
    }
}
