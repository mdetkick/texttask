package com.student.text;

import com.student.text.entity.Text;
import com.student.text.entity.TextComponent;
import com.student.text.logic.TextService;

public class Main {
    public static void main(String[] args) {
        TextComponent text = new Text();
        text.setValue(" asd asd asd ads. asd, asd, asd! asd: asd? aasd.");
        TextService textService = new TextService(text);
        System.out.println(textService.compositeByWordsInSentence().getValue());
        System.out.println(text.getValue());
    }
}
