package com.student.text.entity.character;

import com.student.text.entity.TextComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SemiColon extends TextComponent {
    private static final Logger LOGGER = LogManager.getLogger(SemiColon.class);

    private static final String VALUE = ";";
    private int indexPosition;

    public void setIndexPosition(int indexPosition) {
        this.indexPosition = indexPosition;
        LOGGER.info("Index of {}(;) is: {}", SemiColon.class.getSimpleName(),this.indexPosition);
    }

    @Override
    public String getValue() {
        return VALUE;
    }

    @Override
    public int getIndexPosition() {
        return indexPosition;
    }

    public SemiColon() {
        LOGGER.info("Create new instance {}(;)",SemiColon.class.getSimpleName());
    }
}
