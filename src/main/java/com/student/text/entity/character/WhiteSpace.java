package com.student.text.entity.character;

import com.student.text.entity.TextComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WhiteSpace extends TextComponent {
    private static final Logger LOGGER = LogManager.getLogger(WhiteSpace.class);

    private static final String VALUE = " ";
    private int indexPosition;

    public void setIndexPosition(int indexPosition) {
        this.indexPosition = indexPosition;
        LOGGER.info("Index of {}(| |) is: {}", WhiteSpace.class.getSimpleName(),this.indexPosition);
    }

    @Override
    public String getValue() {
        return VALUE;
    }

    @Override
    public int getIndexPosition() {
        return indexPosition;
    }

    public WhiteSpace() {
        LOGGER.info("Create new instance {}(| |)",WhiteSpace.class.getSimpleName());
    }
}
