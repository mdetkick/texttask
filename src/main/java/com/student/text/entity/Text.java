package com.student.text.entity;

import com.student.text.entity.sentence.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text extends TextComponent {
    private static final Logger LOGGER = LogManager.getLogger(Text.class);

    private String value;
    private List<TextComponent> textComponents;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    public Text() {
        textComponents = new ArrayList<>();
        LOGGER.info("Create new instance Text");
    }

    @Override
    public void addChildComponent(TextComponent textComponent) {
        int index = textComponent.getIndexPosition();
        if(textComponents.size() == 0){
            textComponents.add(textComponent);
            LOGGER.info("Add text component: {}, with index= {}",textComponent.getValue(),index);
        }else{
            for(TextComponent tc:textComponents){
                if(tc.getIndexPosition() == index){
                    try {
                        throw new IllegalArgumentException("Element with such index persist's");
                    } catch (IllegalArgumentException e) {
                        LOGGER.warn("Cant add text component with index= {}, cause: {}",index,e.getMessage());
                    }
                }else {
                    textComponents.add(textComponent);
                    LOGGER.info("Add text component: {}, with index= {}",textComponent.getValue(),index);
                    break;
                }
            }
        }
    }

    @Override
    public void deleteChildComponent(TextComponent textComponent) {
        textComponents.remove(textComponent);
    }

    @Override
    public List<TextComponent> getChildren() {
        return textComponents;
    }

    @Override
    public List<TextComponent> divide() {
        int sentenceIndex = -1;
        Pattern pattern = Pattern.compile("([^.!?]+[.!?])");
        Matcher matcher = pattern.matcher(value);
        while (matcher.find()) {
            sentenceIndex++;
            Sentence sentence = new Sentence();
            sentence.setValue(matcher.group());
            sentence.setIndexPosition(sentenceIndex);
            addChildComponent(sentence);
        }
        LOGGER.info("Text was divided into: {} sentences",sentenceIndex+1);
        return textComponents;
    }
}
