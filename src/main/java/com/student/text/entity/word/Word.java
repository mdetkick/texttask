package com.student.text.entity.word;

import com.student.text.entity.TextComponent;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Word extends TextComponent {
    private static final Logger LOGGER = LogManager.getLogger(Word.class);

    private String value;
    private int indexPosition;

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void setIndexPosition(int indexPosition) {
        this.indexPosition = indexPosition;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getIndexPosition() {
        return indexPosition;
    }
}
