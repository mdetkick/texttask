package com.student.text.entity.sentence;

import com.student.text.entity.Text;
import com.student.text.entity.TextComponent;
import com.student.text.entity.character.*;
import com.student.text.entity.word.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence extends TextComponent {
    private static final Logger LOGGER = LogManager.getLogger(Sentence.class);

    private String value;
    private int indexPosition;
    private List<TextComponent> sentenceComponents;

    @Override
    public void setValue(String value) {
        this.value = value;
        LOGGER.info("Sentence: {}", this.value);
    }

    @Override
    public void setIndexPosition(int indexPosition) {
        this.indexPosition = indexPosition;
        LOGGER.info("Index of {} in text= {}", Sentence.class.getSimpleName(), this.indexPosition);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getIndexPosition() {
        return indexPosition;
    }

    public Sentence() {
        sentenceComponents = new ArrayList<>();
        LOGGER.info("Create new instance {}",Sentence.class.getSimpleName());
    }

    @Override
    public void addChildComponent(TextComponent textComponent) {
        int index = textComponent.getIndexPosition();
        if (sentenceComponents.size() == 0) {
            sentenceComponents.add(textComponent);
            LOGGER.info("Add text component: {}, with index= {}", textComponent.getClass().getSimpleName(), index);
        } else {
            for (TextComponent tc : sentenceComponents) {
                if (tc.getIndexPosition() == index) {
                    try {
                        throw new IllegalArgumentException("Element with such index persist's");
                    } catch (IllegalArgumentException e) {
                        LOGGER.warn("Cant add text component with index= {}, cause: {}", index, e.getMessage());
                    }
                } else {
                    sentenceComponents.add(textComponent);
                    LOGGER.info("Add text component: {}, with index= {}", textComponent.getClass().getName(), index);
                    break;
                }
            }
        }
    }

    @Override
    public void deleteChildComponent(TextComponent textComponent) {
        sentenceComponents.remove(textComponent);
    }

    @Override
    public List<TextComponent> getChildren() {
        return sentenceComponents;
    }

    @Override
    public List<TextComponent> divide() {
        Pattern pattern = Pattern.compile("([A-Za-z]+)|([-!?;:,. ])");
        Matcher matcher = pattern.matcher(value);
        int componentPosition = -1;
        while (matcher.find()) {
            componentPosition++;
            switch (matcher.group()) {
                case ";": {
                    Colon colon = new Colon();
                    colon.setIndexPosition(componentPosition);
                    addChildComponent(colon);
                    break;
                }
                case ",": {
                    Comma comma = new Comma();
                    comma.setIndexPosition(componentPosition);
                    addChildComponent(comma);
                    break;
                }
                case ".": {
                    Dot dot = new Dot();
                    dot.setIndexPosition(componentPosition);
                    addChildComponent(dot);
                    break;
                }
                case " ": {
                    WhiteSpace whiteSpace = new WhiteSpace();
                    whiteSpace.setIndexPosition(componentPosition);
                    addChildComponent(whiteSpace);
                    break;
                }
                case "!": {
                    Exclamation exclamation = new Exclamation();
                    exclamation.setIndexPosition(componentPosition);
                    addChildComponent(exclamation);
                    break;
                }
                case "?": {
                    QuestionMark question = new QuestionMark();
                    question.setIndexPosition(componentPosition);
                    addChildComponent(question);
                    break;
                }
                case "-": {
                    Dash dash = new Dash();
                    dash.setIndexPosition(componentPosition);
                    addChildComponent(dash);
                    break;
                }
                default: {
                    Word word = new Word();
                    word.setValue(matcher.group());
                    word.setIndexPosition(componentPosition);
                    addChildComponent(word);
                }
            }
        }
        LOGGER.info("Sentence was divided into: {} components.", componentPosition + 1);
        return sentenceComponents;
    }

    public int countWords(){
        int count = 0;
        for(TextComponent tc:divide()){
            if(tc instanceof Word){
                count++;
            }
        }
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sentence)) return false;
        Sentence sentence = (Sentence) o;
        return getIndexPosition() == sentence.getIndexPosition() &&
                getValue().equals(sentence.getValue()) &&
                sentenceComponents.equals(sentence.sentenceComponents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue(), getIndexPosition(), sentenceComponents);
    }
}
