package com.student.text.entity;

import java.util.List;

public abstract class TextComponent {
    public void setValue(String value){
        throw new UnsupportedOperationException("Current operation is not support for this object");
    }

    public abstract String getValue();

    public void setIndexPosition(int indexPosition){
        throw new UnsupportedOperationException("Current operation is not support for this object");
    }

    public int getIndexPosition(){
        throw new UnsupportedOperationException("Current operation is not support for this object");
    }

    public void addChildComponent(TextComponent textComponent){
        throw new UnsupportedOperationException("Current operation is not support for this object");
    }

    public void deleteChildComponent(TextComponent textComponent){
        throw new UnsupportedOperationException("Current operation is not support for this object");
    }

    public List<TextComponent> getChildren(){
        throw new UnsupportedOperationException("Current operation is not support for this object");
    }

    public List<TextComponent> divide(){
        throw new UnsupportedOperationException("Current operation is not support for this object");
    }

    public String composite(List<TextComponent> components){
        StringBuilder sentence = new StringBuilder();
        for(int i = 0;i<components.size();i++){
            TextComponent textComponent = components.get(i);
            if(textComponent.getIndexPosition() == i){
                sentence.append(textComponent.getValue());
            }
        }
        return sentence.toString();
    }
}
